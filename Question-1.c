//Write a C program to reverse a sentence entered by user.

#include <stdio.h>
#include <string.h>

void reverse (char s[])
{
    int len=strlen(s);

    printf("\n The reverse sentence: ");

    for (int i=len; i>=0; i--)
    {
        printf("%c", s[i]);
    }
}

int main ()
{
    char s[100];

    printf("Enter a sentence: ");
    fgets(s, 100, stdin);
    reverse(s);
}
